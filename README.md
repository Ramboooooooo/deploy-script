[TOC]

# deploy-script

#### 项目介绍

1. 本系列一键部署脚本适用于开发、测试、预发布、生产环境的快速部署；
2. 本系列脚本适用于 `CentOS 7` 系列操作系统，并在 `CentOS Linux release 7.9.2009 (Core)` 测试通过；
3. 由于种种原因，目前只提供有限的使用权限，暂不进行开源；
4. 若有开源需求，期望者可提供以下 `TODO LIST` 任意一个脚本给作者收纳后即可开源指定需要脚本，此举目的：完善各个领域部署文档。
    * `MySQL MHA`
    * `MySQL MIC`
    * `Rancher`
    * `Kubernetes`

#### 安装教程

##### 一、操作系统配置

| 功能描述        | 配置方式  | 配置教程    | 备注说明       |
|-------------|-------|---------|------------|
| ssh 一键免密登录  | 单向/多向 | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/135413364) | 2025-01-05 |
| rsync 远程同步  | 单向/多向 | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/136189385) | 2025-01-05 |
| yum 源一键更新   | 在线/离线 | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/136211257) | 2025-01-05 |
| CentOS 定时调度 | 超级/普通 | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/136386668) | 2025-01-05 |

##### 二、应用环境配置

| 功能描述           | 配置方式  | 配置教程    | 备注说明       |
|----------------|-------|---------|------------|
| Java 环境一键部署    | 在线/离线 | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/136818358) | 2025-01-05 |
| Maven 环境一键部署   | 在线/离线 | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/136850869) | 2025-01-05 |
| OpenSSL 环境一键部署 | 在线/离线 | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/139303194) | 2025-01-05 |
| 服务器证书一键颁发脚本    | 实时颁发  | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/140553607) | 2025-01-05 |

##### 三、应用组件配置

| 功能描述                           | 配置方式  | 配置教程    | 备注说明       |
|--------------------------------|-------|---------|------------|
| Tomcat 单例一键安装                  | 在线/离线 | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/136916930) | 2025-01-05 |
| Tomcat 多例一键安装                  | 在线/离线 | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/137119689) | 2025-01-05 |
| Docker 引擎离线包采集脚本               | TAR/YUM | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/137465658) | 2025-01-05 |
| Docker 引擎一键安装                  | 在线/离线 | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/137522179) | 2025-01-05 |
| Docker 引擎一键卸载                  | TAR/YUM | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/137542638) | 2025-01-05 |
| Docker Compose 一键安装            | 在线/离线 | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/137471138) | 2025-01-05 |
| Harbor 仓库一键安装                  | 在线/离线 | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/140959281) | 2025-01-05 |
| 镜像仓库认证信息加密初始化脚本           | 在线/离线 | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/141166576) | 2025-01-05 |
| Nginx Echo 插件一键部署              | 在线/离线 | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/139936368) | 2025-01-05 |
| Nginx LuaJIT 插件一键部署            | 在线/离线 | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/139960745) | 2025-01-05 |
| Nginx DEVEL KIT 插件一键部署         | 在线/离线 | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/139966180) | 2025-01-05 |
| Nginx Lua Module 插件一键部署        | 在线/离线 | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/139967855) | 2025-01-05 |
| Nginx Image Thumb 插件一键部署       | 在线/离线 | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/139997294) | 2025-01-05 |
| Stream Lua Nginx Module 插件一键部署 | 在线/离线 | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/140017345) | 2025-01-05 |
| Nginx Naxsi 插件一键部署             | 在线/离线 | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/140021538) | 2025-01-05 |
| Form Input Nginx Module 插件一键部署 | 在线/离线 | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/140023137) | 2025-01-05 |
| Nginx Lua Waf 插件一键部署           | 在线/离线 | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/140174410) | 2025-01-05 |
| Nginx 服务二进制一键安装                | 在线/离线 | [传送门](https://blog.csdn.net/Rambo_Yang/article/details/140277301) | 2025-01-05 |

##### 四、应用程序配置

| 功能描述          | 配置方式  | 配置教程                                                              | 备注说明       |
|---------------|-------|-------------------------------------------------------------------|------------|
| -             | -     | -                                                                 | 2025-01-05 |


###### Elasticsearch 组件配置

* 在线部署
  ```shell
  # 1、下载部署脚本
  sudo wget https://gitee.com/Ramboooooooo/deploy-script/raw/master/component/elasticsearch/install-elasticsearch-stand-alone.sh
  # 2、查看入参说明
  chmod +x install-elasticsearch-stand-alone.sh && ./install-elasticsearch-stand-alone.sh
  # 3、一键部署应用
  ./install-elasticsearch-stand-alone.sh online 7.15.2 /opt/modules environment elasticsearch-single 9200:9300 elasticsearch:elasticsearch:123456 elasticsearch:node102 /opt/data/elasticsearch:/opt/logs/elasticsearch:/opt/snapshot/elasticsearch true
  ```

* 离线部署
  ```shell
  # 1、下载部署脚本
  sudo wget https://gitee.com/Ramboooooooo/deploy-script/raw/master/component/elasticsearch/install-elasticsearch-stand-alone.sh
  # 2、查看入参说明
  chmod +x install-elasticsearch-stand-alone.sh && ./install-elasticsearch-stand-alone.sh
  # 3、一键部署应用（需要提前下载安装包到自定义目录中，如：/usr/local/src）
  ./install-elasticsearch-stand-alone.sh /usr/local/src elasticsearch-7.15.2-linux-x86_64.tar.gz /opt/modules environment elasticsearch-single 9200:9300 elasticsearch:elasticsearch:123456 elasticsearch:node102 /opt/data/elasticsearch:/opt/logs/elasticsearch:/opt/snapshot/elasticsearch true
  ```
    
#### 参与贡献（待定...）

1.  Fork 本仓库
2.  新建 future_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 欢迎砸 ISSUE